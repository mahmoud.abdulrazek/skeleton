# Colors
# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37
# Red
RED='\033[0;31m'
# No Color
NC='\033[0m'

PACKAGES := $(wildcard packages/*)
FEATURES := $(wildcard packages/features/*)
#you should add all required packages here for build_runner command
#Ex: API for Json_Serialiable & Local_Storage
BUILD-RUNNER := packages/api packages/storage

print:
	for feature in $(FEATURES); do \
		echo $${feature} ; \
	done
	for package in $(PACKAGES); do \
		echo $${package} ; \
	done

pods-clean:
	rm -Rf ios/Pods ; \
	rm -Rf ios/.symlinks ; \
	rm -Rf ios/Flutter/Flutter.framework ; \
	rm -Rf ios/Flutter/Flutter.podspec ; \
	rm ios/Podfile ; \
	rm ios/Podfile.lock ; \


get:
	echo "${RED}Moaz will do flutter pub get xD ${NC}"
	flutter pub get
	for feature in $(FEATURES); do \
		cd $${feature} ; \
		echo "Updating dependencies on $${feature}" ; \
		flutter pub get ; \
		cd ../../../ ; \
	done
	for package in $(PACKAGES); do \
		cd $${package} ; \
		echo "Updating dependencies on $${package}" ; \
		flutter pub get ; \
		cd ../../ ; \
	done

fix:
	echo "${RED}Moaz will do flutter pub get xD ${NC}"
	for feature in $(FEATURES); do \
		cd $${feature} ; \
		dart fix --apply ; \
		cd ../../../ ; \
	done
	for package in $(PACKAGES); do \
		cd $${package} ; \
		dart fix --apply ; \
		cd ../../ ; \
	done

upgrade:
	flutter pub upgrade
	for feature in $(FEATURES); do \
		cd $${feature} ; \
		echo "Updating dependencies on $${feature}" ; \
		flutter pub upgrade ; \
		cd ../../../ ; \
	done
	for package in $(PACKAGES); do \
		cd $${package} ; \
		echo "Updating dependencies on $${package}" ; \
		flutter pub upgrade ; \
		cd ../../ ; \
	done

lint:
	flutter analyze

format:
	dart format --set-exit-if-changed .

testing:
	flutter test
	for feature in $(FEATURES); do \
		cd $${feature} ; \
		echo "Running test on $${feature}" ; \
		flutter test ; \
		cd ../../../ ; \
	done
	for package in $(PACKAGES); do \
		cd $${package} ; \
		echo "Running test on $${package}" ; \
		flutter test ; \
		cd ../../ ; \
	done

test-coverage:
	flutter test --coverage
	for feature in $(FEATURES); do \
		cd $${feature} ; \
		echo "Running test on $${feature}" ; \
		flutter test --coverage ; \
		cd ../../../ ; \
	done
	for package in $(PACKAGES); do \
		cd $${package} ; \
		echo "Running test on $${package}" ; \
		flutter test --coverage ; \
		cd ../../ ; \
	done

clean:
	flutter clean
	for feature in $(FEATURES); do \
		cd $${feature} ; \
		echo "Running clean on $${feature}" ; \
		flutter clean ; \
		cd ../../../ ; \
	done
	for package in $(PACKAGES); do \
		cd $${package} ; \
		echo "Running clean on $${package}" ; \
		flutter clean ; \
		cd ../../ ; \
	done

build-runner:
	for package in $(BUILD-RUNNER); do \
		cd $${package} ; \
		echo "Running build-runner on $${package}" ; \
		dart run build_runner build --delete-conflicting-outputs ; \
		cd ../../ ; \
	done

build-web-dev:
	echo "${RED}Moaz will build you a dev flavor web app ^_^ ${NC}"
	flutter build web --release --web-renderer html --dart-define appFlavor=dev
	firebase deploy

build-web-staging:
	echo "${RED}Moaz will build you a staging flavor web app ^_^ ${NC}"
	flutter build web --release --web-renderer html --dart-define appFlavor=staging
	firebase deploy

build-web-production:
	echo "${RED}Moaz will build you a production flavor web app ^_^ ${NC}"
	flutter build web --release --web-renderer html --dart-define appFlavor=production
	firebase deploy

build-apk-dev:
	echo "${RED}Moaz will build you a dev flavor web app ^_^ ${NC}"
	flutter build apk --release --dart-define appFlavor=dev --obfuscate --split-debug-info=build/app/outputs/symbols

build-apk-staging:
	echo "${RED}Moaz will build you a staging flavor web app ^_^ ${NC}"
	flutter build apk --release --dart-define appFlavor=staging --obfuscate --split-debug-info=build/app/outputs/symbols

build-apk-production:
	echo "${RED}Moaz will build you a production flavor web app ^_^ ${NC}"
	flutter build apk --release --dart-define appFlavor=production --obfuscate --split-debug-info=build/app/outputs/symbols

build-windows-dev:
	echo "${RED}Moaz will build you a Dev Flavor Windows App ^_^ ${NC}"
	echo "${RED}Joke of the day : My OS needs curtains ,why ? Because it has Windows xD ${NC}"
	flutter build windows --release --dart-define appFlavor=dev --obfuscate --split-debug-info=build/app/outputs/symbols

build-windows-staging:
	echo "${RED}Moaz will build you a Staging Flavor Windows App ^_^ ${NC}"
	echo "${RED}Joke of the day : My OS needs curtains ,why ? Because it has Windows xD ${NC}"
	flutter build windows --release --dart-define appFlavor=staging --obfuscate --split-debug-info=build/app/outputs/symbols
	




