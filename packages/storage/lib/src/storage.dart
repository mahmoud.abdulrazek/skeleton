import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:path_provider/path_provider.dart';
import 'package:storage/storage.dart';

class Storage {
  Storage() {
    try {} catch (_) {
      throw Exception(
        'You shouldn\'t have more than one [Storage] instance in your '
        'project',
      );
    }
  }

  static const _themePreferenceBoxKey = 'theme-preference-box';

  final HiveInterface _hive = Hive;

  Future<Box<ThemePreferenceCM>> get themeBox => _openHiveBox(
        _themePreferenceBoxKey,
      );

  Future<void> deleteDB() async {
    final directory = await getApplicationDocumentsDirectory();

    await Hive.deleteBoxFromDisk(
      _themePreferenceBoxKey,
      path: directory.path,
    );
  }

  // Generic Hive Box Open Method that gives you the exact T Box
  Future<Box<T>> _openHiveBox<T>(
    String boxKey,
  ) async {
    if (_hive.isBoxOpen(boxKey)) {
      return _hive.box(boxKey);
    } else {
      //Specific Web Platform
      if (kIsWeb) {
        return _hive.openBox<T>(
          boxKey,
        );
      }

      //Other Platforms
      else {
        final directory = await getApplicationDocumentsDirectory();
        return _hive.openBox<T>(
          boxKey,
          path: directory.path,
        );
      }
    }
  }
}
