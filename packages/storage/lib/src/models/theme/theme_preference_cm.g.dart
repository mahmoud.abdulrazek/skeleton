// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'theme_preference_cm.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ThemePreferenceCMAdapter extends TypeAdapter<ThemePreferenceCM> {
  @override
  final int typeId = 4;

  @override
  ThemePreferenceCM read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return ThemePreferenceCM.light;
      case 1:
        return ThemePreferenceCM.dark;
      default:
        return ThemePreferenceCM.light;
    }
  }

  @override
  void write(BinaryWriter writer, ThemePreferenceCM obj) {
    switch (obj) {
      case ThemePreferenceCM.light:
        writer.writeByte(0);
        break;
      case ThemePreferenceCM.dark:
        writer.writeByte(1);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ThemePreferenceCMAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
