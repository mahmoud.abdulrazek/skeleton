import 'package:hive/hive.dart';
import 'package:storage/storage.dart';

part 'theme_preference_cm.g.dart';

@HiveType(typeId: 4)
enum ThemePreferenceCM {
  @HiveField(0)
  light,
  @HiveField(1)
  dark,
}
