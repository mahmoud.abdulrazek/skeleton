import 'dart:async';
import 'dart:developer';
import 'package:api/src/url_builder.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';

typedef UserTokenSupplier = Future<String?> Function();

class Api {
  Api()
      : _dio = Dio(
          BaseOptions(
            headers: {
              'Accept': 'application/json',
              'Language': 'en',
            },
            followRedirects: true,
            validateStatus: (status) => true,
            // contentType: Headers.jsonContentType,
          ),
        ),
        _urlBuilder = UrlBuilder() {
    // _dio.setUpAuthHeaders(userTokenSupplier);
    _dio.interceptors.add(
      LogInterceptor(responseBody: true, requestBody: true),
    );

    _initializeNetworkChecker();
  }

  //Network Checker
  bool isConnectedToNetwork = false;

  void _initializeNetworkChecker() {
    log('Initializing Network Checker Service');
    Connectivity().onConnectivityChanged.listen(
      (ConnectivityResult result) async {
        if (result != ConnectivityResult.none) {
          isConnectedToNetwork =
              await InternetConnectionCheckerPlus().hasConnection;
        }
      },
    );
  }

  final Dio _dio;
  final UrlBuilder _urlBuilder;

  void addUserToken(String token) {
    _dio.options.headers.addAll({
      'Authorization': 'Bearer $token',
    });
  }

  // end of class
}

enum APIRequestType {
  get,
  post,
}
