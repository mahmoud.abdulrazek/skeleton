class InvalidGeneralAPIException implements Exception {
  const InvalidGeneralAPIException(this.message);
  final String message;
}

class InvalidCodeAPIException implements Exception {
  const InvalidCodeAPIException(this.message);
  final String message;
}

class TempBlockAPIException implements Exception {
  const TempBlockAPIException(this.message);
  final String message;
}

class UnAuthenticatedAPIException implements Exception {}

class GenericErrorAPIException implements Exception {}

class InvalidCredentialsAPIException implements Exception {}
