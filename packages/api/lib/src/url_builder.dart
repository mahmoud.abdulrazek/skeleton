import 'package:component_library/component_library.dart';

class UrlBuilder {
  UrlBuilder() {
    initializeBaseUrl();
  }

  String _baseUrl = '';

  void initializeBaseUrl() {
    _baseUrl = AppFlavors().flavorTypeIsDev
        ?
        //TODO : add Dev API
        ''
        : AppFlavors().flavorTypeIsStaging
            ? 'https://chat-backend-test.troylab.org/api'
            :

            //TODO : add Production API
            '';
  }
}
