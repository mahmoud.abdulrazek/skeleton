import 'dart:io';
import 'package:flutter/foundation.dart';

bool kIsLargeScreen =
    kIsWeb || Platform.isLinux || Platform.isWindows || Platform.isMacOS;
