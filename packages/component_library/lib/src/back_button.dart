import 'package:flutter/material.dart';

import 'theme/colors.dart';

class AppBackButton extends StatelessWidget {
  const AppBackButton({Key? key, this.onBackButton}) : super(key: key);

  final VoidCallback? onBackButton;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsetsDirectional.only(
        top: kToolbarHeight,
        start: 24.0,
      ),
      alignment: Alignment.topLeft,
      child: InkWell(
        hoverColor: AppColors.white,
        onTap: onBackButton,
        child: const Icon(
          Icons.arrow_back_ios,
        ),
      ),
    );
  }
}
