import 'app_in_offline_mode_toast.dart';
import 'package:flutter/material.dart';

class AppInOfflineModePageDecorator extends StatelessWidget {
  const AppInOfflineModePageDecorator({
    Key? key,
    required this.isAppOffline,
    required this.child,
  }) : super(key: key);

  final bool isAppOffline;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        if (isAppOffline) const AppInOfflineModeToast(),
        Expanded(child: child),
      ],
    );
  }
}
