const String appFlavorKey = 'appFlavor';
const String appDevKey = 'dev';
const String appStagingKey = 'staging';
const String appProductionKey = 'production';

enum FlavorType {
  dev,
  staging,
  production,
}

class AppFlavors {
  AppFlavors._() {
    _getAppFlavor();
  }

  static final AppFlavors _instance = AppFlavors._();

  factory AppFlavors() => _instance;

  final _appFlavorValue = const String.fromEnvironment(appFlavorKey);

  //default value to be in dev channel
  FlavorType _appFlavorType = FlavorType.dev;

  FlavorType get appFlavorType => _appFlavorType;

  void _getAppFlavor() {
    switch (_appFlavorValue) {
      case appDevKey:
        _appFlavorType = FlavorType.dev;
        break;

      case appStagingKey:
        _appFlavorType = FlavorType.staging;
        break;

      case appProductionKey:
        _appFlavorType = FlavorType.production;
        break;
    }
  }

  bool get flavorTypeIsDev => _appFlavorType == FlavorType.dev;
  bool get flavorTypeIsStaging => _appFlavorType == FlavorType.staging;
  bool get flavorTypeIsProduction => _appFlavorType == FlavorType.production;
}
