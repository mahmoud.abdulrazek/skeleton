import 'package:component_library/component_library.dart';
import 'package:flutter/material.dart';

class AppInOfflineModeToast extends StatelessWidget {
  const AppInOfflineModeToast({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(8.0),
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color: AppTheme.of(context).textStylesError600,
        ),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(Icons.warning_amber_rounded),
            const SizedBox(width: 8.0,),
            Flexible(
              child: Text(
                'Unfortunately you are offline, check your connection!',
                style: AppTextStyles.w600White(
                  AppResponsive.fontSizeDesktopTabletMobile(
                    context,
                    fonSize: 18.0,
                  ),
                ),
              ),
            )
          ],
        ),
    );
  }
}
