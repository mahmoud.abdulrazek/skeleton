import 'package:flutter/material.dart';

void appSnackbar(
  BuildContext context, {
  required String text,
  int durationInSeconds = 3,
}) {
  ScaffoldMessenger.of(context)
    ..hideCurrentSnackBar()
    ..showSnackBar(
      SnackBar(
        content: Text(
          text,
        ),
        duration: Duration(
          seconds: durationInSeconds,
        ),
      ),
    );
}
