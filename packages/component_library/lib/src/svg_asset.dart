import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class _SvgAsset extends StatelessWidget {
  const _SvgAsset(
    this.assetPath, {
    required this.packageName,
    this.width,
    this.height,
    this.fit,
    this.color,
    Key? key,
  }) : super(key: key);

  final String assetPath;
  final String packageName;
  final double? width;
  final double? height;
  final BoxFit? fit;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      'assets/images/$assetPath.svg',
      width: width,
      height: height,
      package: packageName,
      fit: fit ?? BoxFit.contain,
      colorFilter:
          color == null ? null : ColorFilter.mode(color!, BlendMode.srcIn),
    );
  }
}

class AppSvgAsset extends StatelessWidget {
  const AppSvgAsset(
    this.path, {
    required this.packageName,
    this.width,
    this.height,
    this.fit,
    this.color,
    Key? key,
  }) : _isColorizedByIconTheme = false, super(key: key);

  const AppSvgAsset.colorizedByIconTheme(
    this.path, {
    super.key,
    required this.packageName,
    this.width,
    this.height,
    this.fit,
  }) : color = null, _isColorizedByIconTheme = true;

  final String path;
  final String packageName;
  final double? width;
  final double? height;
  final BoxFit? fit;
  final Color? color;
  final bool _isColorizedByIconTheme;

  @override
  Widget build(BuildContext context) {
    return _SvgAsset(
      path,
      width: width,
      height: height,
      packageName: packageName,
      fit: fit,
      color: _isColorizedByIconTheme? IconTheme.of(context).color : color,
    );
  }
}
