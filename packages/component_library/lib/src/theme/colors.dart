import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  //Common Colors
  static const Color white = Color(0xFFFFFFFF);
  static Color shadowColor = const Color(0x14101828);

  //Gray
  static const Color gray25 = Color(0xFFFCFCFD);
  static const Color gray50 = Color(0xFFF9FAFB);
  static const Color gray100 = Color(0xFFF2F4F7);
  static const Color gray200 = Color(0xFFEAECF0);
  static const Color gray300 = Color(0xFFD0D5DD);
  static const Color gray400 = Color(0xFF98A2B3);
  static const Color gray500 = Color(0xFF667085);
  static const Color gray600 = Color(0xFF475467);
  static const Color gray700 = Color(0xFF344054);
  static const Color gray800 = Color(0xFF1D2939);
  static const Color gray900 = Color(0xFF101828);

  //Primary
  static const Color primary25 = Color(0xFFF5F8FF);
  static const Color primary50 = Color(0xFFEEF4FF);
  static const Color primary100 = Color(0xFFE0EAFF);
  static const Color primary200 = Color(0xFFC7D7FE);
  static const Color primary300 = Color(0xFFA4BCFD);
  static const Color primary400 = Color(0xFF8098F9);
  static const Color primary500 = Color(0xFF6172F3);
  static const Color primary600 = Color(0xFF444CE7);
  static const Color primary700 = Color(0xFF3538CD);
  static const Color primary800 = Color(0xFF2D31A6);
  static const Color primary900 = Color(0xFF2D31A6);

  //Error
  static const Color error25 = Color(0xFFFFFBFA);
  static const Color error50 = Color(0xFFFEF3F2);
  static const Color error100 = Color(0xFFFEE4E2);
  static const Color error200 = Color(0xFFFECDCA);
  static const Color error300 = Color(0xFFFDA29B);
  static const Color error400 = Color(0xFFF97066);
  static const Color error500 = Color(0xFFF04438);
  static const Color error600 = Color(0xFFD92D20);
  static const Color error700 = Color(0xFFB42318);
  static const Color error800 = Color(0xFF912018);
  static const Color error900 = Color(0xFF7A271A);

  //Warning
  static const Color warning25 = Color(0xFFFFFCF5);
  static const Color warning50 = Color(0xFFFFFAEB);
  static const Color warning100 = Color(0xFFFEF0C7);
  static const Color warning200 = Color(0xFFFEF0C7);
  static const Color warning300 = Color(0xFFFEC84B);
  static const Color warning400 = Color(0xFFFDB022);
  static const Color warning500 = Color(0xFFF79009);
  static const Color warning600 = Color(0xFFDC6803);
  static const Color warning700 = Color(0xFFB54708);
  static const Color warning800 = Color(0xFFB54708);
  static const Color warning900 = Color(0xFF7A2E0E);

  //Success
  static const Color success25 = Color(0xFF7A2E0E);
  static const Color success50 = Color(0xFFECFDF3);
  static const Color success100 = Color(0xFFD1FADF);
  static const Color success200 = Color(0xFFA6F4C5);
  static const Color success300 = Color(0xFF6CE9A6);
  static const Color success400 = Color(0xFF32D583);
  static const Color success500 = Color(0xFF12B76A);
  static const Color success600 = Color(0xFF039855);
  static const Color success700 = Color(0xFF027A48);
  static const Color success800 = Color(0xFF05603A);
  static const Color success900 = Color(0xFF054F31);
}
