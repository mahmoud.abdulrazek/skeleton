import 'package:flutter/material.dart';

const int kDesktop = 1280;
const int kTablet = 834;
const int kMobile = 450;

class AppResponsive {
  AppResponsive._();

  static Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  static double screenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static double screenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static double widthByFactorDesktopTabletMobile(
    BuildContext context, {
    required double multipliedDesktopFactor,
    required double multipliedTabletFactor,
    required double multipliedMobileFactor,
  }) {
    final width = screenWidth(context);
    if (width >= kDesktop) {
      return width * multipliedDesktopFactor;
    } else if (width > kMobile && width <= kTablet) {
      return width * multipliedTabletFactor;
    } else {
      return width * multipliedMobileFactor;
    }
  }

  static double fontSizeDesktopTabletMobile(
    BuildContext context, {
    required double fonSize,
  }) {
    final width = screenWidth(context);
    if (width >= kDesktop) {
      return fonSize * 1;
    } else if (width > kTablet && width <= kDesktop ||
        width > kMobile && width <= kTablet) {
      if (fonSize >= 60) {
        return fonSize * .8;
      } else {
        return fonSize;
      }
    } else {
      if (fonSize > 40) {
        return fonSize / 2;
      } else {
        return fonSize;
      }
    }
  }
}
