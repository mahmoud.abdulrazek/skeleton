import 'package:component_library/src/theme/colors.dart';
import 'package:flutter/material.dart';

class AppTextStyles {
  AppTextStyles._();

  //هذا بغرض تسهيل كتابة ال TextStyles
  // جميع ال
  //text styles
  // تم ترتيبها تنازليا

  //Primary

  static TextStyle w700Primary900(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      color: AppColors.primary900,
      fontSize: fontSize,
    );
  }

  static TextStyle w700Primary800(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      color: AppColors.primary800,
      fontSize: fontSize,
    );
  }

  static TextStyle w400Primary700(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      color: AppColors.primary700,
      fontSize: fontSize,
    );
  }

  static TextStyle w600Primary700(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.primary700,
      fontSize: fontSize,
    );
  }

  static TextStyle w700Primary600(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      color: AppColors.primary600,
      fontSize: fontSize,
    );
  }

  static TextStyle w600Primary600(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.primary600,
      fontSize: fontSize,
    );
  }

  static TextStyle w600Primary500(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.primary500,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Primary700(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.primary700,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Primary600(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.primary600,
      fontSize: fontSize,
    );
  }

  //Grays

  static TextStyle w700Gray900(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      color: AppColors.gray900,
      fontSize: fontSize,
    );
  }

  static TextStyle w700Gray800(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      color: AppColors.gray800,
      fontSize: fontSize,
    );
  }

  static TextStyle w600Gray800(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.gray800,
      fontSize: fontSize,
    );
  }

  static TextStyle w700Gray600(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      color: AppColors.gray600,
      fontSize: fontSize,
    );
  }

  static TextStyle w600Gray900(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.gray900,
      fontSize: fontSize,
    );
  }

  static TextStyle w600Gray700(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.gray700,
      fontSize: fontSize,
    );
  }

  static TextStyle w600Gray500(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.gray500,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Gray900(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.gray900,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Gray700(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.gray700,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Gray600(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.gray600,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Gray500(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.gray500,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Gray300(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.gray300,
      fontSize: fontSize,
    );
  }

  static TextStyle w400Gray900(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      color: AppColors.gray900,
      fontSize: fontSize,
    );
  }

  static TextStyle w400Gray600(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      color: AppColors.gray600,
      fontSize: fontSize,
    );
  }

  static TextStyle w400Gray300(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      color: AppColors.gray300,
      fontSize: fontSize,
    );
  }

  static TextStyle w400Gray400(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      color: AppColors.gray400,
      fontSize: fontSize,
    );
  }

  static TextStyle w400Gray500(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      color: AppColors.gray500,
      fontSize: fontSize,
    );
  }

  //White
  static TextStyle w600White(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.white,
      fontSize: fontSize,
    );
  }

  static TextStyle w500White(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      color: AppColors.white,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Success500(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.success500,
      fontSize: fontSize,
    );
  }

  static TextStyle w500Error600(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      color: AppColors.error600,
      fontSize: fontSize,
    );
  }

  static w500Gray400(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      color: AppColors.gray400,
      fontSize: fontSize,
    );
  }

  static w900Primary900(double fontSize) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      color: AppColors.primary900,
      fontSize: fontSize,
    );
  }
}
