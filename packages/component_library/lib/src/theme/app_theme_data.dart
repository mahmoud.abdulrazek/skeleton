import 'package:component_library/component_library.dart';
import 'package:component_library/src/theme/decorated_input_border.dart';
import 'package:flutter/material.dart';

const _dividerThemeData = DividerThemeData(
  space: 0,
);

// If the number of properties get too big, we can start grouping them in
// classes like Flutter does with TextTheme, ButtonTheme, etc, inside ThemeData.
abstract class AppThemeData {
  ThemeData get materialThemeData;

  double screenMargin = Spacing.mediumLarge;
  double gridSpacing = Spacing.mediumLarge;

  Color get backgroundColor;
  Color get popUpBackgroundColor;
  Color get textStylesError600;
}

class LightThemeData extends AppThemeData {
  @override
  ThemeData get materialThemeData => throw UnimplementedError();
  @override
  Color get backgroundColor => AppColors.gray100;
  @override
  Color get popUpBackgroundColor => AppColors.white;
  @override
  Color get textStylesError600 => Colors.white;
}

class DarkThemeData extends AppThemeData {
  @override
  ThemeData get materialThemeData => throw UnimplementedError();
  @override
  Color get backgroundColor => AppColors.gray900;
  @override
  Color get popUpBackgroundColor => AppColors.gray900;
  @override
  Color get textStylesError600 => Colors.white;
}

extension ColorSwatch on Color {
  Map<int, Color> _toSwatch() => {
        50: withOpacity(0.1),
        100: withOpacity(0.2),
        200: withOpacity(0.3),
        300: withOpacity(0.4),
        400: withOpacity(0.5),
        500: withOpacity(0.6),
        600: withOpacity(0.7),
        700: withOpacity(0.8),
        800: withOpacity(0.9),
        900: this,
      };

  MaterialColor toMaterialColor() => MaterialColor(
        value,
        _toSwatch(),
      );
}
