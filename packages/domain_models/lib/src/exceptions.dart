class InvalidGeneralException implements Exception {
  const InvalidGeneralException(this.message);
  final String message;
}

class InvalidCodeException implements Exception {
  const InvalidCodeException(this.message);
  final String message;
}

class TempBlockException implements Exception {
  const TempBlockException(this.message);
  final String message;
}

class UnAuthenticatedException implements Exception {}

class GenericErrorException implements Exception {}

class InvalidCredentialsException implements Exception {}
