import 'package:api/api.dart';
import 'package:get_it/get_it.dart';
import 'package:storage/storage.dart';

final sl = GetIt.instance;

Future<void> initDependencyInjection() async {
  // Api
  sl.registerLazySingleton(
    () => Api(),
  );

  // Storage
  sl.registerLazySingleton(
    () => Storage(),
  );

  // Repositories

}
