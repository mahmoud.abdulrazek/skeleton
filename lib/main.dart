import 'dart:async';
import 'dart:developer';
import 'package:skeleton/dependency_injection.dart';
import 'package:skeleton/routing_table.dart';
import 'package:component_library/component_library.dart';
import 'package:device_preview/device_preview.dart';
import 'package:domain_models/domain_models.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

// Hint: Always change this whenever you need to test DevicePreview
const bool isTestingDevicePreview = false;

void main() async {
  runZonedGuarded<Future<void>>(
    () async {
      WidgetsFlutterBinding.ensureInitialized();

      await initDependencyInjection();

      final AppFlavors appFlavors = AppFlavors();

      // Used For Testing Responsivity => remember to set the value to true
      if (isTestingDevicePreview) {
        return runApp(
          DevicePreview(
            enabled: !kReleaseMode,
            builder: (_) => App(
              appFlavorType: appFlavors.appFlavorType,
            ),
          ),
        );
      }

      // Normal Dev life :D
      return runApp(
        App(
          appFlavorType: appFlavors.appFlavorType,
        ),
      );
    },
    (error, stack) {
      log('error: $error $stack');
    },
  );
}

class App extends StatefulWidget {
  const App({
    super.key,
    required this.appFlavorType,
  });

  final FlavorType appFlavorType;

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  //Routing
  late final RoutemasterDelegate _routemasterDelegate = RoutemasterDelegate(
    routesBuilder: (context) => RouteMap(
      routes: buildRoutingTable(
        routemasterDelegate: _routemasterDelegate,
      ),
    ),
  );

  final _lightTheme = LightThemeData();
  final _darkTheme = DarkThemeData();

  @override
  Widget build(BuildContext context) {
    return AppTheme(
      lightTheme: _lightTheme,
      darkTheme: _darkTheme,
      child: ScreenUtilInit(
        useInheritedMediaQuery: isTestingDevicePreview ? true : false,
        designSize: _designSizeByPlatform(),
        builder: (context, child) {
          return MaterialApp.router(
            debugShowCheckedModeBanner: false,
            theme: _lightTheme.materialThemeData,
            darkTheme: _darkTheme.materialThemeData,
            themeMode: ThemeMode.light,
            routerDelegate: _routemasterDelegate,
            routeInformationParser: const RoutemasterParser(),
          );
        },
      ),
    );
  }

  Size _designSizeByPlatform() => const Size(375, 812);
}

extension ThemePreferenceToThemeMode on ThemePreference {
  ThemeMode toThemeMode() {
    switch (this) {
      case ThemePreference.light:
        return ThemeMode.light;
      case ThemePreference.dark:
        return ThemeMode.dark;
    }
  }
}
