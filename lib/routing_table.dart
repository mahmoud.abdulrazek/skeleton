import 'package:routemaster/routemaster.dart';

Map<String, PageBuilder> buildRoutingTable({
  required RoutemasterDelegate routemasterDelegate,
}) {
  return {
    //Splash
    _PathConstants.splashPath: (_) => throw UnimplementedError(),
  };
}

class _PathConstants {
  const _PathConstants._();

  static String get splashPath => '/';
}
